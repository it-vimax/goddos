package main

import (
	"fmt"
	"time"
	"net/http"
	"encoding/json"
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
)

type Timeline struct {
	Id int
	User_id int
	Source_user_id int
	Message string
	Ts int
}

type Res struct {
	Time string `json:"time"`
}


func main() {
	//dsn := "root@tcp(localhost:3306)/social?"
	dsn := "maks:maks1989@tcp(localhost:3306)/social?"
	dsn += "&charset=utf8"
	dsn += "&interpolateParams=true"

	db, err := sql.Open("mysql", dsn)
	db.SetMaxOpenConns(10)
	if err != nil {
		panic(err)
	}

	for {
		res := make(map[int]Res, 50)

		for i := 0; i < 50; i++ {
			t1 := time.Now()
			resp, err := http.Get("http://damask.blutbad.ru/forum.php?m=228051530")
			if err != nil {
				fmt.Println(err.Error())
			}
			t2 := time.Now()
			defer resp.Body.Close()
			confInStr := t2.Sub(t1).String()
			res[i] = Res{Time: confInStr}
		}
		jsonBytes, _ := json.Marshal(res)
		jsonRes := string(jsonBytes)

		result, err := db.Exec("INSERT INTO `test` (`data`) VALUES (?)", jsonRes)
		if err != nil {
			fmt.Println(err.Error())
		}

		affected, err := result.RowsAffected()
		if err != nil {
			fmt.Println(err.Error())
		}
		lastId, err := result.LastInsertId()
		if err != nil {
			fmt.Println(err.Error())
		}
		fmt.Println("insert - RowsAffected", affected, "lastInstId: ", lastId)
	}
}